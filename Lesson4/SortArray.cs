﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson4
{
    internal class SortArray
    {
        public static void NewSort()
        {
            //6. Создайте массив строк.Заполните его произвольными именами людей.
            //Отсортируйте массив.  
            //Результат выведите на консоль.

            Console.WriteLine("How many numbers will the array contain?");
            int quantityNumbers = int.Parse(Console.ReadLine());
            string[] arrayFirstName = new string[quantityNumbers];

            for (int i = 0; i < quantityNumbers; i++)
            {
                Console.WriteLine($"Name number {i+1}?");
                arrayFirstName[i] = Console.ReadLine();
            }

            Console.WriteLine("Got an array:");
            Program.DisplayingArrayElements(arrayFirstName);
            Array.Sort(arrayFirstName);
            Console.WriteLine("Names in sorted order:");
            Program.DisplayingArrayElements(arrayFirstName);
        }
    }
}
