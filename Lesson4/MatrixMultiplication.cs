﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace Lesson4
{
    internal class MatrixMultiplication
    {
        public static void MatrixMultiplicationTwoDimensionalArrays()
        {
            Console.WriteLine("Matrix1:");
            int[,] matrix1 = { { 1, 0, 0, 0 }, { 0, 1, 0, 0 }, { 0, 0, 0, 0 } };
            DisplayingArray(matrix1);
            Console.WriteLine("Matrix2:");
            int[,] matrix2 = { { 1, 2, 3 }, { 1, 1, 1 }, { 0, 0, 0 }, { 2, 1, 0 } };
            DisplayingArray(matrix2);
            int[,] resultMatrix = new int[matrix1.GetLength(0), matrix2.GetLength(1)];

            Console.WriteLine("Result matrix:");
            for (int i = 0; i < matrix1.GetLength(0); i++)
            {
                for (int j = 0; j < matrix2.GetLength(1); j++)
                {
                    for (int k = 0; k < matrix2.GetLength(0); k++)
                    {
                        resultMatrix[i, j] += matrix1[i, k] * matrix2[k, j];
                    }
                    Console.Write($"{resultMatrix[i, j]}\t");
                }
                Console.WriteLine();
            }
        }

        public static void DisplayingArray(int[,] array)
        {
            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    Console.Write($"{array[i, j]}\t");
                }
                Console.WriteLine();
            }
        }
    }
}
