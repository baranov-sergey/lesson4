﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson4
{
    internal class MaxMinArithmeticalMean
    {
        public static void ArithmeticalMeanArrays()
        {
            //3.Создайте 2 массива из 5 чисел.
            //Выведите массивы на консоль в двух отдельных строках.  
            //Посчитайте среднее арифметическое элементов каждого массива и сообщите,
            //для какого из массивов это значение оказалось больше(либо сообщите, что их средние арифметические равны).

            int[] numbers1 = new int[5];
            int[] numbers2 = new int[5];
            Random rnd = new Random();

            for (int i = 0; i < 5; i++)
            {
                numbers1[i] = rnd.Next(10);
                numbers2[i] = rnd.Next(10);
            }
            Console.WriteLine("Got an arrays:");
            Program.DisplayingArrayElements(numbers1);
            Program.DisplayingArrayElements(numbers2);

            double arithmeticalMeanNumbers1 = ArithmeticalMean(numbers1);
            double arithmeticalMeanNumbers2 = ArithmeticalMean(numbers2);

            if (arithmeticalMeanNumbers1 > arithmeticalMeanNumbers2)
            {
                Console.WriteLine($"arithmetical mean array numbers1 = {arithmeticalMeanNumbers1} > arithmetical mean array numbers2 = {arithmeticalMeanNumbers2}");
            }
            else if (arithmeticalMeanNumbers1 < arithmeticalMeanNumbers2)
            {
                Console.WriteLine($"arithmetical mean array numbers1 = {arithmeticalMeanNumbers1} < arithmetical mean array numbers2 = {arithmeticalMeanNumbers2}");
            }
            else
            {
                Console.WriteLine($"arithmetical mean array numbers1 = {arithmeticalMeanNumbers1} == arithmetical mean array numbers2 = {arithmeticalMeanNumbers2}");
            }
        }
        public static double ArithmeticalMean(int[] array)
        {
            double sum = 0;
            foreach (var el in array)
            {
                sum += el;
            }
            double arithmeticalMean = sum / array.Length;
            return arithmeticalMean;
        }
    }
}
