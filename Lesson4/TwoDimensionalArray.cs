﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson4
{
    internal class TwoDimensionalArray
    {
        public static void SumTwoDimensionalArray()
        {
            //8. Создайте двумерный массив целых чисел.Выведите на консоль сумму всех
            //элементов массива.

            Console.WriteLine("Specify the number of rows");
            int rows = int.Parse(Console.ReadLine());
            Console.WriteLine("Specify the number of columns");
            int columns = int.Parse(Console.ReadLine());
            Console.WriteLine();
            int[,] array = new int[rows, columns];
            Random rnd = new Random();
            int sum = 0;

            Console.WriteLine("Got an array:");
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    array[i, j] = rnd.Next(10);
                    Console.Write($"{array[i, j]}\t");
                    sum += array[i, j];
                }
                Console.WriteLine();
            }
            Console.WriteLine("\nSum of all array elements = " + sum);
        }
    }
}
