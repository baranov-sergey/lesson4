﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson4
{
    internal class TwoDimensionalArraySort
    {
        public static void OutputTwoDimensionalArraySort()
        {
            //10. Создайте двумерный массив целых чисел.Отсортируйте элементы в строках
            //двумерного массива по возрастанию.

            Console.WriteLine("Specify the number of rows");
            int rows = int.Parse(Console.ReadLine());
            Console.WriteLine("Specify the number of columns");
            int columns = int.Parse(Console.ReadLine());
            Console.WriteLine();
            int[,] array = new int[rows, columns];
            Random rnd = new Random();

            Console.WriteLine("Got an array:");
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    array[i, j] = rnd.Next(10);
                    Console.Write($"{array[i, j]}\t");
                }
                Console.WriteLine();
            }

            Console.WriteLine("\nSorted array:");
            int[] temp = new int[columns];
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    temp[j] = array[i, j];
                }
                Array.Sort(temp);
                for (int k = 0; k < columns; k++)
                {
                    array[i, k] = temp[k];
                    Console.Write($"{array[i, k]}\t");
                }
                Console.WriteLine();
            }
        }
    }
}
