﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson4
{
    internal class ArrayDiagonals
    {
        public static void OutputArrayDiagonals()
        {
            //9 Создайте двумерный массив.Выведите на консоль диагонали массива.

            Console.WriteLine("Specify the number of rows");
            int rows = int.Parse(Console.ReadLine());
            Console.WriteLine("Specify the number of columns");
            int columns = int.Parse(Console.ReadLine());
            Console.WriteLine();
            int[,] array = new int[rows, columns];
            Random rnd = new Random();
            int sum = 0;

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    array[i, j] = rnd.Next(10);
                    Console.Write($"{array[i, j]}\t");
                    sum += array[i, j];
                }
                Console.WriteLine();
            }

            Console.WriteLine("Output of the first diagonal");
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < i; j++)
                {
                    Console.Write("\t");
                }
                Console.WriteLine($"{array[i, i]}");
            }
            Console.WriteLine("Output of the second diagonal");

            for (int i = 0; i < rows; i++)
            {
                for (int j = rows - 1; j > i; j--)
                {
                    Console.Write("\t");
                }
                Console.WriteLine($"{array[i, rows - 1 - i]}\t");
            }
        }
    }
}
