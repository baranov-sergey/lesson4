﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson4
{
    internal class OddIndexToZero
    {
        public static void OddIndexToZeroInArray()
        {
            //5. Создайте массив и заполните массив.
            //Выведите массив на экран в строку.
            //Замените каждый элемент с нечётным индексом на ноль.
            //Снова выведете массив на экран на отдельной строке. 

            Console.WriteLine("How many numbers will the array contain?");
            int quantityNumbers = int.Parse(Console.ReadLine());
            int[] numbers = new int[quantityNumbers];
            Random rnd = new Random();

            Console.WriteLine("Got an array:");
            for (int i = 0; i < quantityNumbers; i++)
            {
                numbers[i] = rnd.Next(10);
                Console.Write(numbers[i] + " ");
                if (i % 2 != 0)
                {
                    numbers[i] = 0;
                }
            }
            Console.WriteLine("\nZero was written in the array at odd indexes of elements:");
            Program.DisplayingArrayElements(numbers);
        }
    }
}
