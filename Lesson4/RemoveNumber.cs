﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson4
{
    internal class RemoveNumber
    {
        public static void RemoveNumberInArray()
        {
            //1. Создайте массив целых чисел. Удалите все вхождения заданного числа из массива.  
            //Пусть число задается с консоли.Если такого числа нет - выведите сообщения об этом.
            //В результате должен быть новый массив без указанного числа.

            Console.WriteLine("How many numbers will the array contain?");
            int quantityNumbers = int.Parse(Console.ReadLine());
            int[] tempNumbers = new int[quantityNumbers];

            Console.WriteLine("Enter number");
            int number = int.Parse(Console.ReadLine());
            Random rnd = new Random();
            int cnt = 0;

            Console.WriteLine("Got an array:");
            for (int i = 0; i < quantityNumbers; i++)
            {
                tempNumbers[i] = rnd.Next(10);
                if (tempNumbers[i] == number) { cnt++; }
            }
            Program.DisplayingArrayElements(tempNumbers);

            Console.WriteLine($"Array without {number}:");
            int[] numbers = new int[quantityNumbers - cnt];
            int j = 0;
            for (int i = 0; i < quantityNumbers; i++)
            {
                if (tempNumbers[i] != number)
                {
                    numbers[j] = tempNumbers[i];
                    j++;
                }
            }
            Program.DisplayingArrayElements(numbers);

            if (cnt == 0)
            {
                Console.WriteLine($"Number {number} not found in array");
            }
        }
    }
}
