﻿using Lesson4;
using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Immutable;
using System.Reflection;
using System.Text;
using System.Xml;
using static System.Runtime.InteropServices.JavaScript.JSType;

public class Program
{
    private static void Main(string[] args)
    {
        while (true)
        {
            Console.WriteLine(
            "\nWhat task will we choose(number)?\n" +
            "[0] NumberSearchInArray\n" +
            "[1] RemoveNumberInArray\n" +
            "[2] ArithmeticalMeanArrays\n" +
            "[3] MinMediumMaxValueArray\n" +
            "[4] EvenArrayNumbers\n" +
            "[5] OddIndexToZeroInArray\n" +
            "[6] NewSort\n" +
            "[7] BubbleSort\n" +
            "[8] SumTwoDimensionalArray\n" +
            "[9] OutputArrayDiagonals\n" +
            "[10] OutputTwoDimensionalArraySort\n" +
            "[11] MatrixMultiplicationTwoDimensionalArrays\n");

            if (!int.TryParse(Console.ReadLine(), out int task))
            { Console.WriteLine("Incorrect value, please re-enter the operation"); continue; }

            switch (task)
            {
                case 0: NumberSearch.NumberSearchInArray(); break;
                case 1: RemoveNumber.RemoveNumberInArray(); break;
                case 2: MaxMinArithmeticalMean.ArithmeticalMeanArrays(); break;
                case 3: MinMediumMaxValue.MinMediumMaxValueArray(); break;
                case 4: EvenNumbers.EvenArrayNumbers(); break;
                case 5: OddIndexToZero.OddIndexToZeroInArray(); break;
                case 6: SortArray.NewSort(); break;
                case 7: AlgorithmBubbleSort.BubbleSort(CreateArray()); break;
                case 8: TwoDimensionalArray.SumTwoDimensionalArray(); break;
                case 9: ArrayDiagonals.OutputArrayDiagonals(); break;
                case 10: TwoDimensionalArraySort.OutputTwoDimensionalArraySort(); break;
                case 11: MatrixMultiplication.MatrixMultiplicationTwoDimensionalArrays(); break;
                default: Console.WriteLine("Incorrect task number, please re-enter the number"); break;
            }
        }

    }
    public static int[] CreateArray()
    {
        Console.WriteLine("How many numbers will the array contain?");
        int quantityNumbers = int.Parse(Console.ReadLine());
        int[] array = new int[quantityNumbers];
        Random rnd = new Random();

        for (int i = 0; i < quantityNumbers; i++)
        {
            array[i] = rnd.Next(10);
        }
        return array;
    }
    public static void DisplayingArrayElements<T>(T[] array)
    {
        foreach (var el in array)
        {
            Console.Write(el + " ");
        }
        Console.WriteLine();
    }
}

