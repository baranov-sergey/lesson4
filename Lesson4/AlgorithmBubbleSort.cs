﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson4
{
    internal class AlgorithmBubbleSort
    {
        public static void BubbleSort(int[] array)
        {
            //7.Реализуйте алгоритм сортировки пузырьком.

            Console.WriteLine("Unsorted array:");
            Program.DisplayingArrayElements(array);

            for (int i = array.Length - 1; i > 0; i--) // на какое место будем ставить наибольший элемент
            {
                bool flag = false; // false - не было обменов, true - был хотя бы 1 обмен
                for (int j = 0; j < i; j++) // проходим по не отсортированной последовательности
                {
                    if (array[j] > array[j + 1])// если порядок элементов неправильный
                    {
                        // меняем местами пары
                        int temp = array[j];
                        array[j] = array[j + 1];
                        array[j + 1] = temp;
                        flag = true; // меняем флажок
                    }
                }
                if (flag == false) // если значение флага не поменялось
                {
                    break; ; // массив отсортирован. Выходим с функции
                }
            }
            Console.WriteLine("Sorted array:");
            Program.DisplayingArrayElements(array);
        }
    }
}
