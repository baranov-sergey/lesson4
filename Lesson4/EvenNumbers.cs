﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson4
{
    internal class EvenNumbers
    {
        public static void EvenArrayNumbers()
        {
            //4.Создайте массив из n случайных целых чисел и выведите его на экран.
            //Размер массива пусть задается с консоли и размер массива может быть больше 5 и меньше или равно 10.
            //Если n не удовлетворяет условию - выведите сообщение об этом.Если пользователь ввёл не подходящее число, то программа должна просить пользователя повторить ввод.  
            //Создайте второй массив только из чётных элементов первого массива, если они там есть, и вывести его на экран.  

            int quantityNumbers;

            while (true)
            {
                Console.WriteLine("How many numbers will the array contain, choose a value in the range 5 < value <= 10?");
                if (!int.TryParse(Console.ReadLine(), out quantityNumbers))
                { Console.WriteLine("Incorrect value, please re-enter the operation"); continue; }
                if (quantityNumbers <= 5 || quantityNumbers > 10)
                { Console.WriteLine("Incorrect value, please re-enter the operation"); continue; }
                break;
            }

            int[] numbers = new int[quantityNumbers];
            Random rnd = new Random();
            int cnt = 0;

            for (int i = 0; i < quantityNumbers; i++)
            {
                numbers[i] = rnd.Next(10);
                if (numbers[i] % 2 == 0)
                {
                    cnt++;
                }
            }
            Console.WriteLine("Got an array:");
            Program.DisplayingArrayElements(numbers);

            int[] parityNumbers = new int[cnt];

            while (true)
            {
                if (cnt == 0) { Console.WriteLine("There are no even numbers in the array"); break; }

                int j = 0;
                for (int i = 0; i < quantityNumbers; i++)
                {
                    if (numbers[i] % 2 == 0)
                    {
                        parityNumbers[j] = numbers[i];
                        j++;
                    }
                }
                Console.WriteLine("Array of even numbers:");
                Program.DisplayingArrayElements(parityNumbers);
                break;
            }
        }
    }
}
