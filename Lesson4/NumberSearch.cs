﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson4
{
    public  class NumberSearch
    {
        public static void NumberSearchInArray()
        {
            //0.Создайте массив целых чисел.
            //Напишете программу, которая выводит сообщение о том, входит ли заданное число в массив или нет.
            //Пусть число для поиска задается с консоли.

            Console.WriteLine("How many numbers will the array contain?");
            int quantityNumbers = int.Parse(Console.ReadLine());
            int[] numbers = new int[quantityNumbers];

            Console.WriteLine("Enter number");
            int number = int.Parse(Console.ReadLine());
            Random rnd = new Random();
            int cnt = 0;

            Console.WriteLine("Got an array:");
            for (int i = 0; i < quantityNumbers; i++)
            {
                numbers[i] = rnd.Next(10);
                if (numbers[i] == number) { cnt++; }
            }
            Program.DisplayingArrayElements(numbers);


            if (cnt == 0)
            {
                Console.WriteLine($"Number {number} not found in array");
            }
            else
            {
                Console.WriteLine($"The {number} in the array occurs {cnt} times");
            }
        }
    }
}
