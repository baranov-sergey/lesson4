﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson4
{
    internal class MinMediumMaxValue
    {
        public static void MinMediumMaxValueArray()
        {
            //2. Создайте и заполните массив случайным числами и выведете максимальное, минимальное и среднее значение.  
            //Для генерации случайного числа используйте метод Random().Пусть будет возможность создавать массив произвольного размера.
            //Пусть размер массива вводится с консоли.  

            int[] numbers = Program.CreateArray();

            int sum = 0;
            foreach (int number in numbers)
            {
                sum += number;
            }
            Console.WriteLine("Got an array:");
            Program.DisplayingArrayElements(numbers);

            Array.Sort(numbers);
            Console.WriteLine("Sorted array:");
            Program.DisplayingArrayElements(numbers);

            Console.WriteLine("\nMinimum array value = " + numbers[0]);
            Console.WriteLine("Array mean value = " + sum / numbers.Length);
            Console.WriteLine("Maximum array value = " + numbers[numbers.Length - 1]);
        }
    }
}
